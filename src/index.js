import React from 'react';
import ReactDOM from 'react-dom';
import firebase from 'firebase/app';
import './index.css';
import App from './App';
//import { useAuthState } from "react-firebase-hooks/auth";
//import { useCollectionData } from "react-firebase-hooks/firestore";

firebase.initializeApp({
  apiKey: "AIzaSyCqxrYDnGqSLdA24rMuW29T22YgCnsD1Ic",
  authDomain: "myportfolio-25e0c.firebaseapp.com",
  projectId: "myportfolio-25e0c",
  storageBucket: "myportfolio-25e0c.appspot.com",
  messagingSenderId: "602870655563",
  appId: "1:602870655563:web:f5f93ca45670dc44bebbce",
  measurementId: "G-CV2DH3P44Y"
});

//const auth = firebase.auth();
//const firestore = firebase.firestore();

//import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
//reportWebVitals();

import React from 'react';
import Home from './home';
import About from './about';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Blog from './blog';



function App(){


return(

<div>
<Router>
  <Switch>
  <Route path="/" exact component={Home}/>
  <Route path="/about" exact component={About}/>
  <Route path="/blog" exact component={Blog}/>
  <Route path="*" component={Home}/>
  </Switch>
</Router>
</div>
);



}

export default App;
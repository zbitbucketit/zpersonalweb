import React from 'react';
import './index.css';

function Blogit(props){

    function textToHtml(html)
    {
    let arr = html.split(/<br\s*\/?>/i);
    return arr.reduce((el, a) => el.concat(a, <br />), []);
    }

return( 
    <div>
    <article className="blog_article">
    <h2 className="blog_h2" id={"" + props.title}>{props.title}</h2>{"\n\n"}
    <p className="blog_p">{props.head_article}</p>

    {
    props.sub_articles.map(function(subitems){
    return (
    <div>
      <p className="blog_p">{textToHtml(subitems.stitle)}</p>
      <img src={subitems.simage_loc} alt=""/>
    </div>);
    })
    }
    <img src={props.image_loc} alt=""/>
    <p className="blog_p">{props.article_desc}</p>  
    </article>
    </div>
);
}

export default Blogit;
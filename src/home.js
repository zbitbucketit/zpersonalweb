import React from 'react';
import './index.css';

function Home(){
  
document.body.className="home_body"

return(
<div>
<p className="home_p">Zono's Personal web.</p>
<div className="home_demo">&#9733; DEMO version only &#9733; Specially modified for Nest Solutions assesment &#9733;</div>
<p1 className="home_p1">
<div><a className="home_a" href="about">About me</a></div>
<div><a className="home_a" href="blog">Blog</a></div>
<div><a className="home_a" href="product">Product</a></div>
</p1>
<p>
    <b> Used techniques </b><br />
    &#9733; Basic HTML format <br />
    &#9733; Basic CSS Styling <br />
    &#9733; React / JSON / Gatsby <br />
    &#9733; Firebase hosting <br />
    &#9733; Bitbucket version control <br />

</p>
</div>
);
}

export default Home;
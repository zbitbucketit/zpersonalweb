import React, {useState, useEffect} from 'react';
import main12 from './files/main12.jpg';
import main12_ from './files/main12_.jpg';
import Blogit  from "./blog_article.js";


function Blog(){

useEffect(() => { loadJson(); },[]);

const [jdata, setItems] = useState([]);

const loadJson = async () => {
const datas = await fetch('../blog/blog_articles.json').catch(err => { console.log("Error on fetch data:" + err); });

/*
fetch('../blog/blog_articles.json').then(response => {
    console.log(response);
    return response.json();
  }).then(data => {  console.log(data); }).catch(err => { console.log("Error Reading data " + err); });
  console.log(datas);
*/


const jdata = await datas.json().catch(err => { console.log("Error on convert:" + err); });
//console.log(jdata.title);
setItems(jdata);
}
document.body.className="blog_body"
return(
<div>
<header className="blog_header">
    <div className="blog_color_divbox">&#9733;Color on hover</div>
    <img className="blog_img" src={main12_} alt="" 
    onMouseEnter={e => (e.currentTarget.src = main12)}
    onMouseOut={e => (e.currentTarget.src = main12_)}/>

    <h1 className="blog_h1">Zono's JZX blog</h1>
    <ul className="blog_ul">
    {
    jdata.map(function(object, i){ return <li className="blog_li" key={i}>
      <a className="blog_a" href={"#" + object.title} key={i}>{object.title}</a></li> })
    }
    </ul>
  </header>
<div>
{
jdata.map(function(object){
return <Blogit title={object.title} sub_articles={object.sub_articles} head_article={object.head_article} image_loc={object.image_loc} article_desc={object.article_desc}/>
})
}
</div>


<div>

{
// Needs attention on PAGE BOTTOM - style it !!!
}

<font className="blog_h" color="red">
Page is not fully finished. To be updated...
</font>
<font color="red">
Best viewed on Desktop only.

(C) 2019, Zonos.</font>
</div>
</div>
);
}

export default Blog;